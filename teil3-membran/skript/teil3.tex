% !TeX spellcheck = en_US
\documentclass[10pt]{scrartcl}
\usepackage[margin=3cm]{geometry}
\usepackage{xcolor}
\usepackage[utf8]{inputenc}	% Unicode characters
\usepackage[english]{babel} % English language/hyphenation
\usepackage{amsmath}
\usepackage{verbatim}		% multi line comments
\usepackage{tcolorbox}
%\usepackage{cleveref}
\usepackage{acronym}
\usepackage{csquotes}
\usepackage{scalerel}	% scale math symbols e.g. \langle, \rangle 
\usepackage{graphics,wrapfig,lipsum}
\usepackage{cleveref}
% tikz
\usepackage{tikz}
\usetikzlibrary{shapes,arrows,positioning}
\tikzstyle{file} = [draw, text width=6em, text badly centered, node distance=3cm]
\tikzstyle{prog} = [rectangle, draw, fill=blue!20, text width=5em, text badly centered, rounded corners, minimum height=4em]
\tikzstyle{line} = [draw, -latex']

% minted
\usepackage{minted}
\usemintedstyle{emacs}
\definecolor{bg}{rgb}{0.90,0.95,1.00}
\setminted{frame=single}
\setminted{bgcolor=bg}

% own commands
\newcommand{\average}[1]{\ensuremath{\stretchleftright[1000]{\langle}{#1}{\rangle}}}	% average brackets
\newcommand{\gromacs}{\textsc{Gromacs\ }}
\newcommand{\inb}[1]{\mintinline{bash}{#1}}
\newcommand{\inc}[1]{\mintinline{c}{#1}}
\newcounter{task}
\newenvironment{task}
{
	\stepcounter{task}
	\begin{tcolorbox}
	\textbf{Task \thetask:}\\ 
}
{
	\end{tcolorbox}
}

\newcounter{question}
\newenvironment{question}
{
	\stepcounter{question}
	\begin{tcolorbox}[colback=red!10]
	\textbf{Question \thequestion:}\\ 
}
{
	\end{tcolorbox}
}


% header
\title{M.PF1 - Theoriekurs AK van der Vegt}
\subtitle{Part 3: Gas diffusion and solubility in a polymeric membrane}
\date{18 July 2019}
\author{AK van der Vegt}

\begin{document}
\maketitle

\section{Introduction} 

In order to use natural gas for combustion, methane has to be separated from carbon dioxide. 
\begin{wrapfigure}{l}{5cm}
\includegraphics[width=5cm]{pictures/Fig1.pdf}
\end{wrapfigure}
Membranes work on the principle of selective permeation. A scheme of a membrane for gas separation is shown in the Figure. The permeation depends on the diffusion coefficient and the solubility of the gas in the membrane. In this part of the practical, we will try to analyze whether a polymer is suitable to separate methane from carbon dioxide. Also, we will look at what more information can be obtained from computer simulations that are simply not accessible via experiments. \\\\
The permeability of a gas is defined as the product of the diffusion coefficient D times the solubility S:
\begin{equation}
P = D \cdot S
\end{equation}
In the second part of the practical you have seen, that the diffusion coefficient is easily measured in a computer simulation through the mean squared displacement.
\begin{equation}
D = \lim_{t \to \infty} \frac{1}{6t} \langle \left(r(t)-r(t_0)\right)^2 \rangle \label{eq:MSD}
\end{equation}
The solubility can also be obtained through computer simulations via the excess chemical potential.
\begin{equation}
S=\frac{T_0}{p_0 \cdot T} exp\left( -\frac{\mu_{ex}}{RT}\right)
\end{equation}
where T$_0$ and p$_0$ are the standard temperature and pressure (273.15 K and 1 atm).

\section{The polymer}

\begin{wrapfigure}[7]{l}{4cm}
\includegraphics[width=4cm]{pictures/Fig2.pdf}
\end{wrapfigure}

The system you will be analyzing is polydimethylsiloxane (PDMS) chain with ten units and some ester groups at the end. You can find the chemical structure in the figure. In the folder \inb{prac3/Bulk} you will find three sub folder \inb{Box_298}, \inb{Box_308} and \inb{Box_318}, which contain the results of 10 ns long simulations at three different temperatures (298 K, 308 K and 318 K) of a box containing 384 polymer chains. \\
Move in the folder \inb{Box_298} and have a look at the files, which are present. All the necessary information about the interactions and topology for a polymer chain is given in the three files \inb{ffgmx.atp}, \inb{ffgmx.itp} and \inb{ffgmx.rtp}. The polymer chain has a more complicated structure than water, therefore more parameters are needed to describe the interactions and topology. In the \inb{ffgmx.atp} you find the list of the atom types with their masses. In the \inb{ffgmx.itp} file you find the first atom types with the Lennard-Jones parameters and then three sections to describe the so-called bonded interactions: bonds, angles and dihedrals. \\
The bond stretching between two covalently bonded atoms i and j is described by an harmonic potential: \\

\begin{equation}
V_b=\frac{1}{2}k_{ij}(r_{ij}-r_{0,ij})^2
\end{equation}

The potential acting on a triplet of atoms i-j-k is also harmonic:

\begin{equation}
V_a=\frac{1}{2}k_{ijk}(\theta_{ijk}-\theta_{0,ijk})^2
\end{equation}

For the dihedral interactions Gromacs uses a function based on expansion in powers of cos$\phi$, the so called Ryckaert-Bellemans potential:

\begin{equation}
V_d=\sum_{n=0}^5 C_n (cos\phi)^2
\end{equation}

In the \inb{ffgmx.rtp} information on the topology of the system are given. Three units are defined: PDMS - the repeating unit, ENDL - the left end of the chain and ENDR - the right end of the chain. Spend some minutes to have a look at these files and try to understand what they mean. \\
You can now have a look at the final configuration (\inb{confout.gro}) with VMD. Draw in red the PDMS monomer (resname PDMS) and in blue the ends of the chains (resname ENDL or resname ENDR).

\begin{question}
Is the system homogeneous or does it show a phase separation between the PDMS part and the end chain part Why would this be the case
\end{question}

\begin{task}
Obtain the average density of the three boxes and plot them against temperature. How does the density behave with respect to the temperature? To which thermodynamic quantity is the slope of the graph related?
\end{task}

Gromacs has many powerful tools to perform different analysis. One of them is \inb{g_polystat}, which gives some information about typical polymer quantities like the radius of gyration and the end-to-end distance of a polymer. The command is the following: \\

\begin{minted}{bash}
g_polystat -f traj.xtc
\end{minted}

The whole polymer has to be chosen for the analysis.

\begin{question}
Plot the radius of gyration and the end to end distance for the three systems. Comment on the behavior of these two quantities with respect to temperature. What is the definition of the radius of gyration? What is the end to end distance? How are these two quantities related?
\end{question}

\section{Diffusion Coefficients}
Move now to the folder \inb{Diffusion}. You will find inside two folders named \inb{CH4} and \inb{CO2}. In each folder a simulation at 298 K was performed on the same box, you have analyzed before, plus 5 molecules of each gas type. The number 5 was chosen to represent an infinite dilute solution corresponding to a membrane in contact with a low pressure gas. With 5 molecules we can however have a representative statistics, which would not be possible with just one molecule. \\
For each gas, go in the corresponding folder and create a index file that contains each single gas molecule (\inb{make_ndx} option: r + "the number of the residue you can read from the \inb{conf.gro} file"). \\
Then run the \inb{g_msd} program: \\

\begin{minted}{bash}
g_msd -n index.ndx -f traj.xtc -mol -o msd1.xvg 
\end{minted}

Repeat this four more times with the different molecules (change the name of the output files).

\begin{task}
Calculate the diffusion coefficients from the individual MSD for methane and CO$_2$, using equation \ref{eq:MSD}. Then, make an average and calculate the standard deviation.
Is methane diffusing faster than carbon dioxide or is the opposite happening? Calculate the selective diffusion: $D_{CO2}/D_{CH4}$ 
\end{task}

From these two simulations one can try to understand if the gas molecules have an affinity for certain groups of the polymer, by calculating the RDFs. Create and index file with a group that contains both chain ends. (Tip: Gromacs automatically produces a group ENDL and a group ENDR, to merge them type \inb{2 | 4}) 

\begin{question}
Calculate the RDFs for the following interactions: \\
GAS-PDMS \\
GAS-(ENDL and ENDR) \\
Choose one of the five gas molecules for the RDF calculations. Are there any differences in the behavior of the two gases? Are these differences expected?
\end{question}

\begin{wrapfigure}[8]{r}{3cm}
\includegraphics[width=3cm]{pictures/Fig3.pdf}
\end{wrapfigure}

Create an index file with the following type of atoms belonging to end groups: CM, C1, CD, CE, OE1, OE2, CAC. You can find a scheme of the end chain on the right with the corresponding atom types. \\

\begin{question}
Calculate the RDFs between the gas and each of these atoms. What are the major differences between the two gases?
\end{question}

Associate the type names with the chemical structure and try to draw some conclusions, why the differences are located close to specific groups.

\section{Solubilities}

The gas solubility is related to excess chemical potential of the gas molecule through the following equation:

\begin{equation}
S=\frac{T_0}{p_0 \cdot T} \cdot exp\left(\frac{-\mu_{ex}}{RT}\right) \label{eq:sol}
\end{equation}

where S is the solubility, $T_0$=273.15 K, $p_0$=1 atm, T is the temperature at which the simulation is done and $\mu_ex$ is the excess chemical potential. The units of solubility are expressed in $cm^3~(STP)~cm^{-3}~atm^{-1}$. The excess chemical potential can be calculated with the Widom's test particle insertion method (Widom, B., J. Chem. Phys., \textbf{1963}, 39, 2808-2812). A gas particle is inserted randomly at several positions in the polymer matrix and the potential energy between the inserted molecule and the matrix is calculated and weighted through the Boltzmann factor.

\begin{equation}
\mu_{ex}=-k_BT ln\left(\langle exp(-U/k_BT) \rangle_0\right) 
\end{equation}

Input files to calculating solubility of the gases are kept in the folder named \inb{Solubility}. Create a folder called \inb{CH4_298}. Copy inside this folder all the files contained in \inb{input_files_CH4}. Move to the folder \inb{Bulk/Box_298} and run the following command: \\

\begin{minted}{bash}
trjconv -f traj.xtc -o rerun.xtc -b 5000 
\end{minted}

You will obtain a trajectory file with only the last 5 ns of the simulation. Move back to \inb{CH4_298} and copy from \inb{Bulk/Box_298} the file \inb{rerun.xtc}. Copy also from the same folder the file \inb{confout.gro} and call it \inb{conf.gro}. Insert at the end of the file \inb{conf.gro} (but before the size of the box) the lines with a methane molecule you find in \inb{ch4.gro} and change accordingly the number of atoms in the system in the second line of the \inb{conf.gro} file. \\

\begin{minted}{bash}
grompp 
mdrun -rerun rerun.xtc 
\end{minted}

Open the file \inb{tpi.xvg} with \inb{xmgrace}. You will find the evolution of the chemical potential in kJ/mol with time. The value of the excess chemical potential should converge to a plateau. Use this converged value in formula \ref{eq:sol} to obtain the solubility. Repeat the same procedure for the two other temperatures (remember to modify the value of ref\_t in the \inb{grompp.mdp} file) and for carbon dioxide.

\begin{question}
Plot the values of the solubility for methane and carbon dioxide and of the solubility-selectivity $S_{CO2}/S_{CH4}$ as a function of temperature. Which gas is more soluble? What is the influence of temperature? Did you expect the results?
\end{question}

\section{Permeability}

You have now calculated both the diffusion coefficient and the solubility for both gases at T=298~K.

\begin{question}
How large is the permselectivity $P_{CO2}/P_{CH4}$? Is the membrane separation performance stronger influenced by the diffusion or by the solubility? In which directions would you work in order to increase the selective permeability of the membrane?
\end{question}

\section{Report}
The report for this part should consist of the answers to questions 1-6 and the results from task 1 and 2.

\end{document}
