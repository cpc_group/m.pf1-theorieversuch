#!/bin/bash

echo removing the old output file Ttot.dat
rm -f Ttot.dat

# ? ist eine wildcard wie *. Sie steht allerdings für genau ein Zeichen,
# weswegen hier energy.xvg nicht erfasst wird.
#
# Das Konstrukt $(Befehl) gibt die Ausgabe des Befehls als Zeichenkette (String) zurück,
# sodass dieser in der Variable gespeichert werden kann.
energyfiles=$(ls energy?.xvg)

echo The following files will be analyzed:
echo $energyfiles

# Das ist eine Schleife. Die Befehle im Inneren werden für jedes Element in $energyfiles einmal ausgeführt
for file in $energyfiles;
do
    echo Now analyzing file $file
    awk 'BEGIN {count=0;tot=0} {tot=tot+$4; count=count+1} END {print "temperature = " tot/count}' $file >> Ttot.dat
done
