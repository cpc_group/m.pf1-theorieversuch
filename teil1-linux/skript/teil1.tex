% !TeX spellcheck = de_DE
\documentclass[10pt]{scrartcl}
\usepackage[margin=3cm]{geometry}
\usepackage{xcolor}
\usepackage[utf8]{inputenc}	% Unicode characters
\usepackage[ngerman]{babel} % English language/hyphenation
\usepackage{amsmath}
\usepackage{csquotes}
\usepackage{verbatim}		% multi line comments
\usepackage{tcolorbox}
\usepackage{cleveref}

% code coloring stuff
\usepackage{minted}
\usemintedstyle{emacs}
\definecolor{bg}{rgb}{0.90,0.95,1.00}
\setminted{frame=single}
\setminted{bgcolor=bg}

% own commands
\newcommand{\gnulinux}{GNU/Linux\ }
\newcommand{\inb}[1]{\mintinline{bash}{#1}}
\newcommand{\inc}[1]{\mintinline{c}{#1}}
\newcounter{task}
\newenvironment{task}
{
	\stepcounter{task}
	\begin{tcolorbox}
	\textbf{Aufgabe \thetask:}\\
}
{
	\end{tcolorbox}
}

\newcounter{question}
\newenvironment{question}
{
	\stepcounter{question}
	\begin{tcolorbox}[colback=red!10]
	\textbf{Frage \thequestion:}\\
}
{
	\end{tcolorbox}
}

% header
\title{M.PF1 - Theoriekurs AK van der Vegt}
\subtitle{Teil 1: Einführung in Linux }
\date{03 August 2017}
\author{AK van der Vegt}

\begin{document}
\maketitle

\tableofcontents

\section{Einführung}

\gnulinux ist ein UNIX-artiges Betriebssystem. Im Gegensatz zu anderen gängigen Betriebssystemen (z.B. MS Windows, MacOs, etc ) kann der Quelltext der Programme kostenlos aus dem Internet runtergeladen werden (\enquote{Open Source}).

Darüber hinaus ist \gnulinux \enquote{Freie Software}, das heißt es ist dem Benutzer nicht nur erlaubt, sich alles anzusehen und frei zu benutzen, sondern auch Teilprogramme oder das ganze Betriebssystem anzupassen. Dies führte dazu, dass sich eine große Gemeinschaft an Programmierern fand, die kontinuierlich das System weiterentwickelten. Allgemein ist die Philosophie hinter \gnulinux eine möglichst große Transparenz und Offenheit. Oft spricht man nur von Linux ohne das GNU, auch wenn das eigentlich nur den Kern des Betriebssystems meint. \gnulinux ist die Grundlage von Android und sehr verbreitet auf Webservern, in der Wissenschaft und anderen professionellen Anwendungen.

Das Betriebssystem Linux unterstützt nativ mehrere Benutzer auf dem selben Rechner (Multi-User) und mehrere Prozesse gleichzeitig (Multi-Tasking) ohne dass diese sich gegenseitig in die Quere kommen. Eines der Prinzipien von UNIX-Systemen ist \enquote{everything is a file}, was bedeutet, dass fast alles über das Dateisystem ansprechbar ist. Ein anderes Prinzip ist \enquote{the right tool for the right job}: Für jede Aufgabe gibt es ein passendes Programm, dass diese Aufgabe möglichst gut erfüllt, aber keine unnötigen Funktionen bereitstellt.

Durch die Offenheit sind verschiedene Linux-Distributionen entstanden, welche sich hauptsächlich durch ihre unterschiedlichen graphischen Oberflächen unterscheiden (im Praktikum wird Ubuntu verwendet, welches aktuell Unity als Oberfläche nutzt).

In der graphischen Oberfläche kann man Programme und Dateien durch Klicken, Ziehen und Menüauswahlen verwalten. Dennoch ist es in vielen Fällen schneller und bei einigen Programmen sogar notwendig mit der textbasierten Eingabe, der sogenannten \enquote{Shell}, zu arbeiten. Sie liest die vom Benutzer eingegebene Kommandos, interpretiert diese und gibt (falls erzeugt) Resultate auf dem Bildschirm aus. Da die Shell keine graphische Hilfestellung zur Verfügung stellt, muss der Benutzer die Kommandos kennen. Die Shell selbst ist das Programm, dass eure Befehle interpretiert. Es wird über ein Terminal-Fenster angesprochen. Im Praktikum wird die Bash-Shell benutzt, welche unter \gnulinux am weitesten verbreitet ist

\section{Arbeiten in der Shell}
\subsection{Login}

Den Loginname und das dazugehörige Passwort erhältst du zu Beginn des Praktikums von der Assistent\_in.

\begin{task}
Logge dich ein. Dieser Prozess kann beim ersten mal etwas länger dauern. Mache dich anschließend mit der graphischen Oberfläche vertraut (Klicke herum - trial and error). Öffne schließlich ein Terminal-Fenster, darin öffnet sich automatisch eine neue Shell.
\end{task}

\subsection{Grundlegende Kommandos}
In einer neuen Shell sieht man einen kurzen Text, der euch verschiedene Dinge angibt. Dieses sogenannte \enquote{prompt} Kommando besteht meistens aus

\begin{itemize}
	\item dem Benutzernamen
	\item einem at-Zeichen (@)
	\item dem Namen des Computers
	\item einem Doppelpunkt
	\item dem aktuellen Verzeichnis (\inb{~} entspricht eurem Home Verzeichnis)
	\item und einem Dollar Zeichen (\$).
\end{itemize}

Dahinter steht der Curser, dargestellt durch ein Rechteck, wo der Benutzer nun das gewünschte auszuführende Kommando eingibt und mit Enter ausführt.

Die meisten Programme haben einen sehr ähnlichen Aufbau:
\begin{minted}{bash}
programm -option1 -option2 ... argument1 argument2 ...
\end{minted}

Um genau zu sein sind die meisten der folgenden Befehle eigene kleine eigenständige Linux-Standardprogramme, die nur eine ganz bestimmte Aufgabe ausführen, zum Beispiel eine Datei zu löschen. Im Folgenden wird der Einfachheit halber von Befehlen oder Kommandos gesprochen.

\subsection{Manuals}

\begin{task}
Führe die folgenden (optionslosen und argumentlosen) Kommandos aus. Versuche zu erraten, was ihr Zweck ist.
\begin{minted}{bash}
whoami
pwd
\end{minted}
\end{task}

Zu den meisten grundlegenden Kommandos gibt es Anleitungen (manuals). Diese können mit dem Kommando \inb{man} aufgerufen werden, wobei der zu untersuchende Befehl als Argument dem Befehl angehängt wird. Im Manual kann man mit den Pfeiltasten nach oben und nach unten scrollen. \texttt{q} beendet das Manual-Programm.

\begin{task}
Prüfe nach ob deine Interpretation der Befehle richtig war.
\begin{minted}{bash}
man whoami
man pwd
\end{minted}
\end{task}

\subsection{Bewegen in den Datenverzeichnissen}

Dateien sind in einem Computer in einem Baum angeordnet, der von \inb{/} ausgeht (ähnlich wie \inb{C:\ } unter Windows). Ohne einen Dateibrowser, den es auch unter Ubuntu gibt, den ihr aber für dieses Praktikum nicht braucht, kann man schnell den Überblick verlieren. Eines der nützlichsten Kommando damit dies nicht geschieht ist \inb{ls}.

\begin{task}
Studiere den Eintrag im Manual für \inb{ls}. Dieses Kommando besitzt sehr viele verschiedene Optionen. Führe mit diesem Wissen dann die unten stehenden
Kommandos aus.
\begin{minted}{bash}
ls
ls -a
ls -l
ls -l -a
ls -a -l
ls -la
\end{minted}
\begin{question}
Was tun sie? Welche sind identisch?
\end{question}
\end{task}

Das für Nutzer wichtigste Verzeichnis ist das Heimverzeichnis (home directory). Es ist das Hauptverzeichnis eines jeden Benutzers. Zu allem was
darüber liegt hat er im Normalfall nur Leserechte (kann aber die Dateien nicht verändern oder löschen).

Das Verzeichnis in dem Ihr euch aktuell befindet ist das Arbeitsverzeichnis. Der Befehl \inb{pwd} gibt dessen absoluten Pfad zurück und es wird in der Prompt angezeigt. Ein Programm wird im Arbeitsverzeichnis aufgerufen und ein Dokument im aktuellen Verzeichnis kann direkt bearbeitet werden.

Um das Arbeitsverzeichnis zu wechseln, verwendet man das Kommando \inb{cd}. Als Argument gibt man den Pfad ein zu dem man will. Der kann absolut sein (beginnt mit \inb{/} und enthält alle Zwischenverzeichnisse; vgl. \inb{pwd}) oder relativ (startet im aktuellen Arbeitsverzeichnis). Letzteres ist meist viel schneller und zweckmäßiger.

\begin{task}
Führe die folgenden cd Befehle in der entsprechenden Reihenfolge aus. Nutze zwischendurch \inb{ls} um dir den Inhalt des Verzeichnisses anzusehen.
\begin{minted}{bash}
cd Documents
cd
cd ..
cd .
cd /
cd /home/student/m.pf1-theorieversuch
cd ~
cd ~/Documents
cd ../Downloads
\end{minted}
\begin{question}
Wofür stehen die Platzhalter \inb{.}, \inb{..} und \inb{~}? Welche der obigen Kommandos bewirken dasselbe?
\end{question}
\end{task}

Tipp 1: Die meisten Shells unterstützen Tab-Vervollständigung: Gib einmal \inb{cd ~/Doc} ein und drücke auf die Tab-Taste (eventuell zweimal). Das kann das Arbeiten in der Shell stark beschleunigen.

Tipp 2: Die Platzhalter können nicht nur für \inb{cd} eingesetzt werden, sondern auch in allen anderen Kommandos, die einen Pfad benötigen (z.B. \inb{ls ..})! Merk sie dir, dann musst du nicht jedes mal das Arbeitsverzeichnis wechseln, wenn du zum Beispiel nur kurz nachschauen willst, welche Dateien im nächsthöheren Verzeichnis liegen.

\subsection{Verwalten von Verzeichnissen und Dateien}

Zum Verwalten der Daten auf einem Computer gehört das Erstellen, Kopieren, Verschieben, Umbenennen und Löschen von Verzeichnissen und Datein.

\begin{task}
Die Kommandos für die oben beschriebenen Tätigkeiten lauten \inb{mkdir}, \inb{rmdir}, \inb{cp}, \inb{rm} und \inb{mv}. Finde heraus (mit \inb{man}), was diese Kommandos tun, wie sie auf Datein und Verzeichnisse wirken und was die Option \inb{-r} bei \inb{cp} und \inb{rm} bewirken.

\begin{question}
Wie lauten die Befehle um folgende Dinge zu tun?
\begin{itemize}
	\item Das Verzeichnis \inb{~/Documents} mit Inhalt nach \inb{~/Old_Documents} zu kopieren.
	\item Das Verzeichnis \inb{~/Old_Documents} in \inb{~/old-documents} umzubenennen.
	\item Das Verzeichnis \inb{~/old-documents} samt Inhalt zu löschen.
\end{itemize}
\end{question}
\end{task}

Alle diese Kommandos erlauben die Verwendung eines unbestimmten Platzhalters \texttt{*}. Damit lassen sich z.B. all Dateien mit \texttt{.txt} am Ende des Namens löschen: \inb{rm *.txt}

Eine saubere Einteilung in Unterverzeichnisse wird in diesem Praktikum strikt empfohlen. Sie erleichtert es bei den vielen sich akkumulierenden Daten den Überblick zu behalten. Als Vorschlag: Richte für jeden Praktikumsteil ein eigenes Verzeichnis ein.

\begin{task}
Denk dir einen einmaligen Gruppennamen aus und lege folgendes Verzeichnis an \inb{~/<jahr>-<gruppenname>}. Dabei ist \inb{<jahr>} ist das aktuelle Jahr und \inb{<gruppenname>} euer Gruppenname (beides ohne Klamern). Kopiere den Ordner \inb{~/m.pf1-theorieversuch/teil1-linux/daten} mit allen enthaltenen Dateien nach \inb{~/<jahr>-<gruppenname>/teil1}. In diesem Projektordner wirst du nun weiterarbeiten.\newline
Erledige dasselbe gleich auch noch für \texttt{teil2} und \texttt{teil3}.
\end{task}

\subsection{Zugriffsrechte}\label{sec:zugriffsrechte}
\gnulinux ist der Philosophie entsprechend ein offenes System. Das heißt, die meisten Dateien sind für alle zugänglich. Dabei werden drei verschiedene Zugriffsrechte (Permissions) unterschieden: lesen, schreiben und ausführen. In den meisten Fällen erlaubt man allen anderen Benutzern nur die eigenen Dateien zu lesen (beinhaltet das Recht diese in ein eigenes Verzeichnis zu kopieren, wo jene diese dann falls gewünscht für sich bearbeiten können.). Das Recht zum Schreiben und zum Ausführen, behält man sich selber vor, um nicht plötzlich Überraschungen zu erleben (weil jemand Fremdes die eigenen Dateien gelöscht hat). Das Recht Systemdatein zu verändern hat nur der Administrator, der unter Linux \textit{root} heißt.

Merkt euch: Ihr habt meist nur die Erlaubnis Dateien zu verändern, die in eurem Heimverzeichnis liegen. Die Fehlermeldung „permission denied“ zeigt, dass ihr etwas beschreiben wollt, das ihr nicht dürft (probiert zum Beispiel mal aus ein Verzeichnis in \inb{/etc} zu erstellen).

Die Zugriffsrechte und der Dateibesitzer (owner) lassen sich mit ls –l anzeigen. Dabei zeigen der zweite, dritte und vierte Buchstabe an, was der Besitzer der Datei mit ihr machen darf (die anderen Buchstaben, bestimmen was andere Nutzer dürfen). Dabei steht
\begin{itemize}
	\item r für read (lesen),
	\item w für write (schreiben) und
	\item x für execute (ausführen).
\end{itemize}
Steht dort stattdessen ein Minus, habt ihr das entsprechende Recht nicht. Beispiel Zeile von \inb{ls -l}:\newline
\texttt{-\framebox{rw-}r--r-- 1 \framebox{student} student 8980 Jul 31 08:44 conf.gro}\newline
Diese Datei gehört dem Benutzer Student und er darf sie lesen und schreiben aber nicht ausführen.

Ausführrechte benötigt ihr nur bei Ordnern und bei Skript-Dateien.

Für das Praktikum braucht ihr nur zwei Befehle zu kennen, von denen der Erste eine Datei les- und schreibbar macht. Der Zweite macht sie les-, schreib- und ausführbar.
\begin{minted}{bash}
chmod u=rw dateiname
chmod u=rwx dateiname
\end{minted}

\section{Arbeiten mit Dateien}

\gnulinux kennt mehrere Programme um den Inhalt von Dateien anzuzeigen. Um ihn zu verändern braucht man Editoren, von denen es ebenfalls mehrere gibt. In diesem Teil lernt ihr einige nützliche Programme für diese Aufgaben kennen.

\subsection{Anzeigen von Dateiinhalten}

\begin{task}
Teste die folgenden Kommandos an ein paar der kopierten Dateien in eurem Projektordner.
\begin{minted}{bash}
cat filename
more filename
head -n zahl filename
tail -n zahl filename
grep CO PDMS_CO.gro
\end{minted}
Tipp 1: Statt \texttt{zahl} musst du eine Zahl eingeben.\\
Tipp 2: Verwende \texttt{q} zum Beenden von \inb{more}.
\begin{question}
Was bewirken die Befehle jeweils?
\end{question}
\end{task}

\subsection{Bearbeiten von Dateiinhalten}

\subsubsection{gedit}

Wir konzentrieren uns nun auf die Datei \inb{PDMS_CO.gro}. Sie wurde mit dem Simulationpaket GROMACS erstellt, das im weiteren Verlauf des Praktikums verwendet werden wird. Um nur das Polymermolekül im Graphikprogramm VMD anzuschauen, muss von dieser Datei das Kohlenstoffdioxid entfernt werden und etwas modifiziert werden. Dazu verwenden wir zunächst \inb{gedit}.
\begin{minted}{bash}
gedit PDMS_CO.gro
\end{minted}

Wenn ein Programm von der Shell aus gestartet wurde, blockiert es diese. Laufende Programme können mit meist mit der Tastenkombination \texttt{Ctrl~+~c} (in der Konsole) beendet werden. Ausnahmen bilden zum Beispiel die Programme \inb{vi} (\cref{sec:vi}) welches mit \texttt{:q} und \inb{vmd} (\cref{sec:visual}) welches mit \texttt{quit} beendet werden.

\begin{task}
Öffne \inb{gedit} in der Konsole und schließe es per Tastenkombination. Führe nun \inb{sleep 2m} (Bash-Befehl für 2~min nichts tun) aus und brich auch diesen Befehl ab.
\end{task}

\begin{task}
Die Zeilen die CO enthalten (am Ende der Datei), sollen gelöscht werden und die Anzahl der Atome (zweite Zeile) muss entsprechend geändert werden.

Modifiziere die Datei entsprechend mit \inb{gedit} und speichere die modifizierte Datei unter \inb{PDMS1.gro}.
\end{task}

\subsubsection{vi}\label{sec:vi}

Eine andere Methode die File zu bearbeiten ist die Verwendung von \inb{vi} (bzw. \inb{vim}). Dieser Editor
hat verschiedene Modi, von denen die wichtigsten zwei die folgenden sind:
\begin{enumerate}
	\item Den Operationsmodus (normal mode) der genutzt wird um durch die Datei zu navigieren, Zeilen zu löschen, die Datei zu speichern und das Programm zu beenden. Dieser Modus ist Standard und an der leeren unteren Zeile zu erkennen.
	\item Der Eingabemodus (insert mode), der genutzt wird, um Text am Cursor einzugeben. Ihr erkennt diesen Modus am \texttt{-- INSERT --} in der unteren Zeile.
\end{enumerate}

Einige wichtige Tastenkombinationen für \inb{vi} sind:

\begin{tabbing}
\hspace{1cm}\=\hspace{2cm}\=\\
Im Eingabemodus:\\
\>\texttt{ESC}\>Wechselt in den Operationsmodus\\
Im Operationsmodus:\\
\>\texttt{i}\>Wechselt in den Eingabemodus\\
\>\texttt{dd}\>Löscht eine ganze Zeile\\
\>\texttt{x}\>Löscht das Zeichen unter dem Cursor\\
\>\texttt{.}\>Wiederholt die letzte Veränderung\\
\>\texttt{u}\>Macht die letzte Veränderung rückgängig\\
\>\texttt{gg}\>Springt zum Anfang der File\\
\>\texttt{G}\>Springt ans Ende der File \\
\>\texttt{/wort}\>Sucht nach wort. \texttt{n} springt zum Nächsten\\
\>\texttt{:wq}\>Speichert die geöffnete Datei und beendet vi \\
\>\texttt{:w fname}\>Speichert die Datei unter fname\\
\>\texttt{:q!}\>Beendet vi ohne zu speichern
\end{tabbing}
Es gibt natürlich noch viel, viel mehr, aber mit diesen Befehlen kann man schon genauso effektiv arbeiten wie in z.B. \inb{gedit}.

Vor jedes Kommando im Operationsmodus kann außerdem mit einer Zahl angegeben werden, wie oft das Kommando ausgeführt werden soll. Zum Beispiel wird \texttt{10dd} zehn Zeilen löschen. Oder ein Befehl kann mit einer \enquote{Bewegung} kombiniert werden. Der Befehl \texttt{dt.} löscht Zeichen bis zum nächsten Punkt (\textbf{d}elete \textbf{t}ill \textbf{.}) Diese kombinierten Kommandos machen \inb{vi} so mächtig und wegen ihnen lohnt es sich auf lange Sicht diesen Editor zu lernen.

\begin{task}
Kopiere \inb{PDMS_CO.gro} nach \inb{PDMS2.gro}, öffne letztere Datei dann mit \inb{vi} und entferne wieder CO wie in der letzten Aufgabe.

Überprüfe anschließend mit
\begin{minted}{bash}
diff PDMS1.gro PDMS2.gro
\end{minted}
ob die Dateien identisch sind. Ist dies der Fall erzeugt \inb{diff} keine Ausgabe, falls Unterschiede bestehen, werden diese angezeigt. Finde und korrigiere in dem Fall die Fehler.
\end{task}

\subsection{Visualisierungsporgramme}\label{sec:visual}

\subsubsection{Xmgrace}
Um Messreihen, wie zum Beispiel die Temperatur während einer Simulation, zu plotten kann man verschiedene Tools benutzen. Gromacs (das Simulationsprogramm, das morgen vorgestellt wird) gibt \inb{.xvg} Dateien aus, welche von der Software Xmgrace verstanden werden. Leider ist die Software schon etwas älter, nicht ganz intuitiv zu bedienen und erzeugt nicht besonders schön aussehende Plots. Dafür wird oft automatisch eine Achsenbeschriftung erzeugt und das Format direkt verstanden.

Im Skript wird die Verwendung von Xmgrace beschrieben, aber im Rest des Praktikum ist es freigestellt, welche Software ihr benutzt um Graphen zu plotten und Kurven zu fitten.

\begin{task}
Probiere folgenden beiden Befehle und das Python-Script aus um eine Kurve zu plotten.
\begin{minted}{bash}
xmgrace temp-equi.xvg
\end{minted}
\begin{minted}{bash}
gnuplot -p -e 'set datafile commentschars "#@&";
plot "temp-equi.xvg" using 1:2 with lines'
\end{minted}
\begin{minted}{python}
import numpy as np
import matplotlib.pyplot as plt
data = np.loadtxt("temp-equi.xvg", comments=['#', '@'])
plt.plot(data[:,0], data[:,1])
plt.show()
\end{minted}

Tipp: Um das Python-Script zu starten, gibt es zwei Möglichkeiten:\\ 1. Mit \inb{python} den interaktiven Modus von Python betreten und dort die Befehle ausführen (\texttt{quit()} zum verlassen).\\ 2. Die Befehle in eine Datei \inb{script.py} schreiben und dann mit \inb{python script.py} ausführen.

\end{task}

\subsubsection{VMD}
Mit der 3D-Molekül-Visualisierungssoftware VMD lassen sich Konfigurationen darstellen. Tatsächlich kann die Software noch viel mehr. Über die Konsole kann das Programm direkt mit einer \texttt{.gro} Datei geöffnet werden.
\begin{minted}{bash}
vmd file.gro
\end{minted}

Das Programm unterstützt eine graphische Oberfläche mit interaktiven Werkzeugleisten.

\begin{task}
Versuche durch Klicken herauszufinden, wie das Programm funktioniert. Öffne die Datei \inb{PDMS1.gro} und betrachtet das Molekül von verschiedenen Seiten und in verschiedenen Darstellungen (Graphics - Representations - Drawing Method).
\end{task}

Die Datei \inb{PDMS1.gro} enthält eine einzige Konfiguration der Moleküle in der Box. Bei einer Simulation erhält man viele solcher Konfigurationen, welche aneinandergehängt einen kleinen \enquote{Film} ergeben. Diese nennt man Trajektorie einer Simulation. Sie wird in einer separaten Datei gespeichert.

\begin{task}
Öffnet nun zusätzlich die Trajektorie und lass die Abfolge der Konfigurationen als Film abspielen.
\begin{minted}{bash}
vmd PDMS1.gro traj.xtc
\end{minted}
\end{task}

\section{awk- und bash-Scripting}

\subsection{awk} \label{sec:awk}
Neben klassischen Editoren gibt es einige speziellere Werkzeuge um Dateien zu verändern oder auszuwerten. Eines davon ist \inb{awk}, welches die Zeilen einer Datei durchgeht und jeweils Befehle ausführt. Dabei wird die eigene awk-Programmiersprache verwendet, welche einfach zu lesen und verstehen ist. Meist wird \inb{awk} wie folgt aufgerufen:
\begin{minted}{bash}
awk 'Befehle' input.txt > output.txt
\end{minted}
Dabei gehört \inb{>} nicht zu awk, sondern zur Shell und kann hinter jeden Befehl gehängt werden. Es sorgt dafür, dass die Ausgabe (standard output) von \inb{awk} in \inb{output.txt} geschrieben wird, sonst würde sie schlicht im Terminal erscheinen.

In der File \inb{energy.xvg} sind folgende Ergebnisse einer Simulation zu finden: erste Spalte: Zeit (in ps), zweite Spalte Energie (in kJ/mol), dritte Spalte Dichte (in kg/m$^3$), vierte Spalte Temperatur (in K).


\begin{question}
Was passiert bewirken folgende Kommandos? Was steht in \inb{temp.xvg} und in \inb{temp_C.xvg}?
\begin{minted}{bash}
awk '{print $4}' energy.xvg > temp.xvg
awk '{print ($4-273.15)}' energy.xvg > temp_C.xvg
\end{minted}
\end{question}

\begin{task}
Versuch die Dateien mit Xmgrace zu visualisieren.
\end{task}

In Linux können Befehle auch kombiniert werden. Die \enquote{pipe} \inb{|} leitet die Ausgabe (standad output) von einem Programm an die Eingabe (standard input) des nächsten Programm weiter. Das kombinierte Nutzen von awk, sort und tail kann zum Beispiel zur Ermittlung der 10 höchsten Energiewerte der Simulation dienen.
\begin{minted}{bash}
awk '{print $2}' energy.xvg | sort -n | tail -n 10 > energy_high.xvg
\end{minted}

\begin{task}
Erstellt eine Datei \inb{density_low.xvg} mit den zehn kleinsten Werten der Dichte (Man kann \inb{sort -rn | tail} oder \inb{sort -n | head} benutzen).
\begin{question}
Wie lautet die Befehlszeile, die dafür benötigt wird?
\end{question}
\end{task}

Mit awk kann man viele Rechnungen mit den Daten einer File durchführen ohne dass man eine (nicht so einfach zu erlenende) Programmiersprache wie C oder FORTRAN braucht.

\begin{task}
Berechnet die mittlere Dichte aus der Datei \inb{energy.xvg} mit folgendem Befehl:
\begin{minted}{bash}
awk 'BEGIN {count=0;tot=0} {tot=tot+$3; count=count+1}
END{print tot/count}' energy.xvg
\end{minted}

Tipp: Ihr könnt das in eine oder zwei Zeilen schreiben, Bash erkennt, dass ihr mit dem Befehl noch nicht fertig seid daran, dass das einfache Anführungszeichen nach der ersten Zeile noch nicht geschlossen wurde.

\begin{question}
Was ist der Wert der durchschnittliche Dichte? Erkläre knapp wie der Befehl funktioniert und welche Rolle die Variablen \texttt{tot} und \texttt{count} spielen?.
\end{question}
\end{task}

\subsection{Bash-Scripts}

Wenn man mehrere Dateien zu analysieren hat, ist es sinnvoller ein kleines Script vorzubereiten, als jeweils einen Befehl zu tippen. Ein Beispiel ist das File \inb{mean-temp-all.sh}.

Die Datei ist ein Bash-Script, was man an der ersten Zeile im Script erkennt. Da Bash auch die Shell ist in der du arbeitest, könntest du auch den Inhalt des Skripts Zeile für Zeile abtippen, mit dem selben Ergebnis. Zeilen die mit \texttt{\#} beginnen werden von der Bash-Shell übrigends ignoriert. Daher werden sie als Kommentarzeilen verwendet.
\begin{minted}{bash}
# Kommentar
befehl -option argument
# noch ein Kommentar
\end{minted}

\begin{task}
Macht die Datei gegebenenfalls ausführbar (siehe \cref{sec:zugriffsrechte}) und führt sie dann aus.
\begin{minted}{bash}
./mean-temp-all.sh
\end{minted}
Schaue dir die Ergebnisse in \inb{Ttot.dat} an.\newline
Schaue dir nun das Skript an und versucht es zu verstehen. Es werden der neue Befehl \inb{echo} und Variablen verwendet.

\begin{question}
Wie lautet die erste Zeile eines Bash-Skripts?\newline
Was bewirken die folgenden Zeilen jeweils?
\begin{minted}{bash}
echo hallo
variable="hallo"
echo $variable
\end{minted}
Was bewirkt \inb{>>} im Vergleich zu \inb{>} (bekannt aus \cref{sec:awk})?
\end{question}
\end{task}

\begin{task}
Kopiere das Skript zu \inb{highest-temp-all.sh} und modifiziere das Skript, sodass dieses von allen \texttt{.xvg} Dateien den Dateinamen und die höchste Temperatur darin direkt auf die Konsole ausgibt.
\end{task}

\section{Bericht}

Vom Versuchsbericht ist die Linux-Einführung Teil~1 und besteht selbst aus zwei Unterpunkten:

\begin{enumerate}
\item Einer Zusammenfassung von Linux, seinen Programmen und seinen Werkzeugen auf EINER A4 Seite. Beschränkt Euch dabei auf die, für diesen Praktikumsteil und für den weiteren Verlauf, wichtigsten Komponenten. Dies sollte für eine andere Gruppe eine Kurzzusammenfassung sein, mit der sie möglichst gut für das Praktikum vorbereitet wären.
\item Den Antworten auf die Fragen 1-\thequestion.
\end{enumerate}

\section{Zusatzmaterial}

Einen ähnlichen Einstieg in Linux bietet die erste \textbf{Übung von B.COM}, zu finden im Ordner \inb{~/m.pf1-theorieversuch/teil1-linux/skript/zusatzmaterial}. In dieser wird ausführlicher auf \inb{vi} und Xmgrace eingegangen, sowie einige weitere Kommandos vorgestellt (\inb{sed}, \inb{ps}, \inb{kill}, \inb{top}).

Im selben Ordner findet sich auch ein \textbf{Cheatsheet} zu Bash, vom selben Kurs, welches für die folgenden Teile hilfreich sein könnte. Ein Cheatsheet bietet einen Überblick über die wichtigsten Befehle in kondensierter Form.

Zum vollständigeren Einstieg in \inb{vi} bzw. \inb{vim} kann der interaktive \textbf{VIM Tutor} verwendet werden. Dieser wird mit dem Befehl \inb{vimtutor} gestartet.

\end{document}
