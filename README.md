# M.PF1 Theorieversuch

This counts for two experiments in the M.PF1 practical course and is only for studends who did not do B.COM in their bachelor.

The the trajectories of part 2 and 3 are NOT on github, as those are large. But they are stored on the csi palenque file server under cpc/teaching/m.pf1-theorieversuch. Copy the files from there, if you need them for the students.
