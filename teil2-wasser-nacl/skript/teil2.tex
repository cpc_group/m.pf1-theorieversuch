% !TeX spellcheck = en_US
\documentclass[10pt]{scrartcl}
\usepackage[margin=3cm]{geometry}
\usepackage{xcolor}
\usepackage[utf8]{inputenc}	% Unicode characters
\usepackage[english]{babel} % English language/hyphenation
\usepackage{amsmath}
\usepackage{verbatim}		% multi line comments
\usepackage{tcolorbox}
\usepackage{cleveref}
\usepackage{acronym}
\usepackage{csquotes}
\usepackage{scalerel}	% scale math symbols e.g. \langle, \rangle 

% tikz
\usepackage{tikz}
\usetikzlibrary{shapes,arrows,positioning}
\tikzstyle{file} = [draw, text width=6em, text badly centered, node distance=3cm]
\tikzstyle{prog} = [rectangle, draw, fill=blue!20, text width=5em, text badly centered, rounded corners, minimum height=4em]
\tikzstyle{line} = [draw, -latex']

% minted
\usepackage{minted}
\usemintedstyle{emacs}
\definecolor{bg}{rgb}{0.90,0.95,1.00}
\setminted{frame=single}
\setminted{bgcolor=bg}

% own commands
\newcommand{\average}[1]{\ensuremath{\stretchleftright[1000]{\langle}{#1}{\rangle}}}	% average brackets
\newcommand{\gromacs}{\textsc{Gromacs\ }}
\newcommand{\inb}[1]{\mintinline{bash}{#1}}
\newcommand{\inc}[1]{\mintinline{c}{#1}}
\newcounter{task}
\newenvironment{task}
{
	\stepcounter{task}
	\begin{tcolorbox}
	\textbf{Task \thetask:}\\ 
}
{
	\end{tcolorbox}
}

\newcounter{question}
\newenvironment{question}
{
	\stepcounter{question}
	\begin{tcolorbox}[colback=red!10]
	\textbf{Question \thequestion:}\\ 
}
{
	\end{tcolorbox}
}


% header
\title{M.PF1 - Theoriekurs AK van der Vegt}
\subtitle{Teil 2: Simulation of an aqueous solution containing NaCL with GROMACS}
\date{07 August 2017}
\author{AK van der Vegt}

\begin{document}
\maketitle

\section*{Acronyms}
\begin{acronym}[ECU]
	\acro{MD}{molecular dynamics}
\end{acronym}

\section{Introduction} 

\gromacs is a molecular dynamics simulation package, free for academic purposes, originally developed in Groningen, The Netherlands. The reference website is http://www.gromacs.org. Molecular dynamics (MD) simulation is a technique by which one generates the atomic trajectory of a system of N particles by numerical integration of Newton's equation of motion, for a specific interatomic potential. By modifying the equation of motion, simulations can be performed also at constant temperature (NVT ensemble) or at constant pressure and temperature (NPT). \gromacs offers the possibility to perform MD simulations, but also to analyze the output files generated and get information on the static and dynamical properties of the system.

A regular \gromacs \ac{MD} simulation needs three input files and will output three input files. This is illustrated in \cref{fig:flowchart}.
\begin{figure}[htp]
\centering	
\begin{tikzpicture}
% Place nodes
\node [file] (in2) {run parameters (grompp.mdp)};
\node [prog, right=1cm of in2] (grompp) {grompp};
\node [file, above of=in2] (in1) {topology (topol.top)};
\node [file, below of=in2] (in3) {start coordinates (conf.gro)};
\node [file, right=1cm of grompp] (tpr) {binary input (topol.tpr)};
\node [prog, right=1cm of tpr] (mdrun) {mdrun};
\node [file, right=1cm of mdrun] (out2) {energies (energy.edr)};
\node [file, above of=out2] (out1) {trajectory (traj.trr/.xtc)};
\node [file, below of=out2] (out3) {end coordinates (confout.gro)};

\draw [line] (in1) -- (grompp);
\draw [line] (in2) -- (grompp);
\draw [line] (in3) -- (grompp);
\draw [line] (grompp) -- (tpr);
\draw [line] (tpr) -- (mdrun);
\draw [line] (mdrun) -- (out1);
\draw [line] (mdrun) -- (out2);
\draw [line] (mdrun) -- (out3);
\end{tikzpicture}
\caption{A flowchart of files and programs used for a regular \ac{MD} simulation with \gromacs.}
\label{fig:flowchart}
\end{figure}

The input files are a \texttt{.gro} file which contains the initial coordinates of the systems, a \texttt{.top} file which contains the information about the interactions between the atoms and an \texttt{.mdp} file which contains the simulation parameters (length of the simulation, temperature, pressure, ...). The output files are the following: a \texttt{.gro} file which contains the final coordinates of the system, a \texttt{.trr} or \texttt{.xtc} file, which is the trajectory of the system, contains the information on the position and the velocity of the particles at different times during the simulation, a \texttt{.edr} file, containing information on the energy, the density, the pressure, the temperature of the system.

All these files have standard names, which you can see in \cref{fig:flowchart} and in the manual of the two \gromacs commands (\inb{man grompp}, \inb{man mdrun}). It usually makes sense to stick with the standard names, because then you don't have to tell \gromacs, that you used different filenames.

In this part of the Praktikum, by simulating an aqueous solution containing NaCl, you will get to understand how \gromacs and MD work, how to prepare the input files and how to analyze the ouput files.

\section{Running a simulation}

You will find all the following files in \inb{~/m.pf1-theorieversuch/teil2-wasser-nacl/daten}. If you haven't done it yesterday, copy them into your own project folder.

\subsection{Topology}
In this section parts of the topology file \inb{topol.top} will be explained. Note: In topology files and run parameter files of \gromacs a semicolon (\texttt{;}) is used for comments. In topology files these comments are often used to help the reader understand the meaning of columns.

\subsubsection{Atom types}
\begin{minted}{text}
[ atomtypes ]
;name  mass      charge   ptype  sigma        epsilon
Cl     35.453    -1.000   A      0.440        0.470        ;Weerasinghe
Na     22.9898   1.000    A      0.245        0.320        ;Weerasinghe
OW     15.99940  -0.8476  A      3.15061e-01  6.36386e-01  ;AMBER03/spc/e
HW     1.00800   0.4238   A      0.0          0.0          ;spc/e
\end{minted}
This is the description of the atom types present in the system. The mass is given and also the charge and two parameters sigma and epsilon used to calculate the non-bonded interactions.

The non-bonded interactions are the sum of the electrostatic term and the Lennard-Jones Potential
\begin{equation}\label{eq:potential}
V_{ij}(r) = \frac{q_i q_j}{4 \pi \epsilon_0 r_{ij}} + 4\epsilon_{ij} \left[\left(\frac{\sigma_{ij}}{r_{ij}}\right)^{12} - \left(\frac{\sigma_{ij}}{r_{ij}}\right)^{6}\right]
\end{equation}
where $q_i$ and $q_j$ are the charges of atoms $i$ and $j$, $\epsilon_0$ is the vacuum permittivity, $\epsilon_{ij}$ is the well depth of the Lennard-Jones potential, $\sigma_{ij}$ is the point where the Lennard-Jones potential is zero and $r_{ij}$ is the distance between two atoms i and j.

\begin{question}
The Lennard-Jones potential has two contributions, one attractive and one repulsive. Which one is which? Which are the physical interactions/forces that are modeled by those terms?
\end{question}

\subsubsection{Nonbonded parameters}
\begin{minted}{text}
[nonbond_params]
;i  j   func  sigma     epsilon
Na  OW  1     0.277831  0.33845
\end{minted}

Usually the nonbonded interactions between different atoms are calculated from the parameters $\sigma$ and $\epsilon$ of the atoms by a combination rule. In some cases, however, to have a better description of the system, specific $\sigma_{ij}$ and $\epsilon_{ij}$ are used to describe the interaction between two atoms, like in this case for sodium and the oxygen of water.

\subsubsection{Molecule types}
\begin{minted}{text}
#include "Na.itp"
#include "Cl.itp"
#include "water.itp"
\end{minted}
These three lines indicate that the program will read these additional files, similar as if their content would be inserted in \inb{topol.top}. The files are present in the same folder. Each of them describes a molecule type.

As an example take a look at the content of \inb{water.itp}.
\begin{minted}{text}
; .itp file for SPC/E rigid water

[moleculetype]
;Name  nrexcl
SOL    2

[ atoms ]
;nr  type  resnr  residu  atom  cgnr  charge   mass
1    OW    1      SOL     OW    1     -0.8476  15.99940
2    HW    1      SOL     HW1   1     0.4238   1.00800
3    HW    1      SOL     HW2   1     0.4238   1.00800

[ settles ]
;OW  funct  doh  dhh
1    1      0.1  0.16330
\end{minted}

First a molecule type with the name \texttt{SOL} is created and that atoms connected by 2 or less bonds do not interact with non-bonded interactions.

Then the atoms of \texttt{SOL} are defined. The important thing to notice is that the hydrogen atoms have a name (HW1 and HW2) which is unique and a type (HW), which is the same for both atoms. Different atoms in the system can have the same type and have therefore have the same Lennard-Jones parameters, $\sigma$ and $\epsilon$.

The SPC/E water model used in this simulation is a rigid model, which means that the distances O-O (\texttt{doh}) and H-H (\texttt{dhh}) are kept fixed by an algorithm that is called SETTLE.

\begin{question}
From the bond lengths defined here, what is the H-O-H angle in SPC/E water?
\end{question}

Have a look now the content of the files Na.itp and Cl.itp:

\subsubsection{System}
\begin{minted}{text}
[ system ]
NaCl aqueous solution

[ molecules ]
;mol_name number
Na 100
Cl 100
SOL 11111
\end{minted}

The Header [ system ] indicates the name of the system, in this case we will simulate a box containing water and NaCl. The Header [ molecules ] indicates
which type of molecules and how many of them are present in the system. In this case there are  100 sodium ion, 100 chloride ion and 11111 water molecules which are called SOL.


\subsection{Start coordinates}

Have a look at \inb{conf.gro}.

\begin{minted}{text}
NaCl aqueous solution
33533
1Na      Na    1   1.004   4.329   3.523  0.2385  0.0119  0.2130
2Na      Na    2   3.448   1.099   0.256 -0.0646 -0.1288 -0.4075
3Na      Na    3   6.086   5.558   6.847 -0.3173 -0.3332 -0.2757
...
11311SOL     OW33531   0.680   1.756   1.806  0.5492  0.1437  0.4334
11311SOL    HW133532   0.597   1.739   1.752  1.1977 -2.1136  0.0294
11311SOL    HW233533   0.656   1.761   1.902 -0.1733  0.4782  0.2424
6.91311   6.91311   6.91311
\end{minted}

The first line of the file is the title. The second line contains the total number of atoms of the system. Then the atoms are listed in a fixed format for each line.
\begin{itemize}
	\item molecule number (5 positions)
	\item molecule name (5 characters)
	\item atom name (5 characters)
	\item atom number (5 positions)
	\item position (in nm, x y z in 3 columns, each 8 positions with 3 decimal places)
	\item velocity (in nm/ps, x y z in 3 columns, each 8 positions with 4 decimal places) 
\end{itemize}

The velocities are optional. The last line contains the dimensions of the simulation box in nm.

\subsection{Run parameters}

Have a look at \inb{grompp.mdp}.

\begin{minted}{text}
; Run control
integrator = md
dt = 0.002 ; ps
nsteps = 5000
\end{minted}

This section contains information on the type and length of the simulation.

\begin{minted}{text}
; Output control
nstxout = 0
nstvout = 0
nstfout = 0
nstlog = 100
nstenergy = 100
nstxtcout = 500
\end{minted}
The frequencies with which will be written to the output files.

\begin{minted}{text}
; Neighbor searching
cutoff-scheme = Verlet
rlist = 1
\end{minted}
Information about neighbor searching.

\begin{minted}{text}
; Electrostatics
coulombtype = PME
rcoulomb = 1.0

; Van der Waals
vdwtype = cutoff
rvdw = 1.0
\end{minted}
These sections contain details on the way non-bonded interactions (electrostatic and Lennard-Jones) are calculated.

\begin{minted}{text}
; Temperature coupling
tcoupl = v-rescale
tc-grps = System
ref_t = 298
tau_t = 0.1

; Pressure coupling
Pcoupl = berendsen
Pcoupltype = isotropic
ref_p = 1
tau_p = 1
compressibility = 4.5e-5
\end{minted}
This two sections refer to the coupling of the system to a thermostat and a barostat, to control temperature and pressure. This way an isothermal-isobaric (NPT) ensemble is simulated. The simulation will be run at 298~K and 1~bar.

\begin{task}
Report shortly with help of the \gromacs manual the meaning of each line presented in this section. This can be done in table form.
\end{task}

\subsection{Starting the simulation}

Two steps are needed to start a simulation with \gromacs.

\begin{minted}{bash}
grompp
\end{minted}
By using this command, the \gromacs preprocessor reads the molecular topology file, checks its validity, checks if the simulation parameter make sense, checks the start coordinates and creates a binary file \inb{topol.tpr}, which summarizes all the information contained in the three input files. Additionally a file named \inb{mdout.mdp} is created, which lists the values of all parameters, also the ones that have not been explicitly set in \inb{grompp.mdp}.

\begin{minted}{bash}
mdrun -v
\end{minted}
This is the command that actually runs the simulation. \inb{-v} is an option to make the program tell you how far the simulation is being run. 

The output files generated are \inb{confout.gro}, \inb{ener.edr} and \inb{traj.xtc}.

\section{Analysis}

\subsection{Equilibrium}

By analyzing the file \inb{ener.edr}, one can have a look at how quantities like temperature, pressure, total energy, potential energy, density vary during the time of the simulation.

\begin{minted}{bash}
g_energy -o temp.xvg
\end{minted}
type \texttt{temperature} or \texttt{7} and then press enter two times.

You will obtain a file called \inb{temp.xvg}, which contains the value of the temperature at each frame that has been saved. Also the program will show the mean value and the RMSD.

You can visualize the output file with xmgrace.
\begin{minted}{bash}
xmgrace temp.xvg
\end{minted}

Try to create the following files: \inb{press.xvg}, \inb{dens.xvg}, \inb{tot-energy.xvg} and \inb{pot-energy.xvg}, which contain the time evolution of the pressure, density, total and potential energy of the system, respectively.

\begin{question}
What are the average values and the RMSD of these quantities?
\end{question}

You can also have a look at the final configuration with vmd.
\begin{minted}{bash}
vmd confout.gro
\end{minted}
You will at first see just water molecules. In the Main Menu go to Graphics and then to Representations. Click on Create Rep and substitute \texttt{all} with \texttt{resname Na} and click on Apply. Select as Drawing Method VDW. You will now see the Sodium atoms. Do the same for the Chloride atoms.

\begin{task}
Save the picture and add it to your report by clicking on the main menu on \texttt{File}, then \texttt{Render...} and then \texttt{Start Rendering}.
\end{task}

Tip 1: You can try to find the setting for the background color first and set it to white, in order to save some ink.

Tip 2: The produced picture is in the targa format. You can open it with \inb{eog} and save it as a \texttt{.jpg} file.

\subsection{Radial distribution functions}

In order to analyze the structure of a system, one needs to quantify the distribution of the particles in space and the correlation between their positions. The radial distribution function, (or pair correlation function) $g(r)$ in a system of particles, describes how density varies as a function of distance $r$ from a reference particle (see \cref{fig:rdf-counting-sphere}). The radial distribution function gives an insight of the local environment surrounding a certain particle. An example for a Lennard Jones fluid is given in \cref{fig:rdf-lj}.

\begin{figure}[htp]
	\begin{minipage}[b]{0.4\textwidth}
	\centering
	\includegraphics[width=\linewidth]{pictures/rdf-counting-sphere}
	\caption{Spheres used for the calculation of $g(r)$.}
	\label{fig:rdf-counting-sphere}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.4\textwidth}
	\centering
	\includegraphics[width=\linewidth]{pictures/rdf-lj}
	\caption{Radial distribution function for a Lennard-Jones fluid at liquid state.}
	\label{fig:rdf-lj}
	\end{minipage}
\end{figure}

The peaks represent the position of the neighboring particles. The effect of packing as seen in \cref{fig:rdf-lj} leads to multiple \enquote{solvation shells}, of which the first 4 are visible in this case. Radial distribution functions of homogeneous systems converge to the value of 1 for large distances, where the effect of the local environment vanishes.

Gromacs has a function called \inb{g_rdf}, that calculates radial distribution functions between atoms or groups of atoms. Before one has to create an index file that holds groups (selections) of atoms, which we will later choose from. The function to be used is \inb{make_ndx}.
\begin{minted}{bash}
make_ndx -f topol.tpr
\end{minted}

Some selections of atoms already exists 
\begin{enumerate}
	\setcounter{enumi}{-1}
	\item System (all the atoms)
	\item Ion (Na and Cl)
	\item Na (just Na atoms)
	\item Cl (just Cl atoms)
	\item Water (the water molecules)
	\item SOL (the same as 4)
	\item non-Water (the same as 1)
	\item Water\_and\_ions (the same as 0)
\end{enumerate}

We need to create two new groups, one containing just the oxygens of the water molecule and one containg the hydrogens. Therefore type \texttt{t OW} and enter, then \texttt{t HW} and enter twice.

You have now created a file \inb{index.ndx}, which contains all the subgroups of atoms we are interested in.

Now we look at the distribution function between two oxygen atoms:
\begin{minted}{bash}
g_rdf -f traj.xtc -n index.ndx -o rdf_O_O.xvg -cn rdf_cn_O_O.xvg -bin 0.01
\end{minted}

The program will ask you which groups you want to use. You can now have a look at the rdf with \inb{xmgrace}.

\begin{question}
At which distance does the first peak appear? How does this distance compare with the Lennard-Jones radius used in the model? Are there other peaks?
\end{question}

\inb{g_rdf} produces also a second file \inb{rdf_cn_O_O.xvg}, which is the spatial integral of the radial distribution function. In other words it gives the average number of particles within a distance r.

Read from xmgrace the distance at which there is a valley between the first peak and the second peak in the \inb{rdf_O_O.xvg} file. Read for this distance the value in \inb{rdf_cn_O_O.xvg} (you can use \inb{vi}).

\begin{question}
Which value do you find? What is the physical meaning of it in terms of the water structure (Hint: solvation shells)?
\end{question}

\begin{question}
Calculate the radial distribution function between oxygen and hydrogen! What is the physical meaning of the distance of the first peak? Does it represent a special kind of interaction?
\end{question}

\begin{question}
Calculate now the radial distribution functions first between sodium and oxygen and then between chloride and oxygen. Plot them together with xmgrace. Are there any differences? Are the differences expected? Use the \inb{rdf_cn} files to calculate the coordination number for Na and Cl. Do you obtain the same number? Explain why there are or are no differences.
\end{question}

\begin{question}
Calculate now the radial distribution function between sodium and chloride. Which is the distance for the direct contact between the ions? Could you try to guess the physical meaning of the second and third peak?
\end{question}


\subsection{Diffusion Coefficient}

The diffusion coefficient is related to the mean squared displacement (\textit{MSD}) of a particle in the following way
\begin{equation}\label{eq:diff-coeff}
D = \lim\limits_{t \to \infty} \frac{1}{6t} \mathit{MSD}(t).
\end{equation}
It can be calculated as an average over all $N$ particles distance from their position at time $t_0$
\begin{equation}\label{eq:msd}
\mathit{MSD}(t) = \frac{1}{N} \sum_i^N \left(r_i(t) - r_i(t_0)\right)^2.
\end{equation}

The mean squared displacement as a function of time is characterized by a ballistic regime at very small times, where it is proportional to $t^\alpha$ with $\alpha = 2$, a sub-diffusive regime at small times ($\alpha < 1$), followed by the diffusive regime where MSD is linear proportional to the time ($\alpha = 1$, see the limes in \cref{eq:diff-coeff}).

In order to see the sub-diffusive regime a simulation of the same system has been performed, where the trajectory frames have been saved ten times more often. You can find the trajectory saved as \inb{traj-hires.xtc}. We can now try to calculate the self-diffusion coefficient of water.

There is a function in Gromacs which calculates the mean square displacement which is called \inb{g_msd}. Use the index file again and select the oxygen atoms interactively.

\begin{minted}{bash}
g_msd -f traj-hires.xtc -n index.ndx -o msd-O.xvg
\end{minted}

\begin{task}
Repeat the above to also create \inb{msd-H.xvg} with the MSD of the hydrogen atoms. Plot both and explain similarities.
\end{task}

\begin{task}
Use \inb{awk} to generate a file \inb{msd-O-loglog.xvg} where both the x and y value are logarithmized. Use an editor to remove the invalid lines in the beginning of \inb{msd-O-loglog.xvg}. Then plot the file, make a linear fit of the diffusive part and check the slope. For this you can use \inb{xmgrace} or any tool you want (LibreOffice Calc [installed], Excel, Origin, ...).

For linear fitting in \inb{xmgrace} define a region first under Edit/Regions/Define\dots and then make a regression by clicking on Data/Transformations/Regression\dots .
\end{task}

\begin{question}
What is the slope of the fit in the double logarithmic plot of the MSD? Is this as expected?
\end{question}

\begin{task}
Plot the MSD of oxygen (or hydrogen) in normal scale and make a linear fitting through the times where the regime is diffusive. Report the self diffusion coefficient of water in cm$^2$/s. Calculate the MSD for sodium and chloride and also make a fit for them. Show the three graphs with the fitting functions in one figure.
\end{task}

\begin{question}
Are the ions diffusing faster or slower than water? Which ion is faster? How would you expect the diffusion coefficients to change when increasing the simulation temperature?
\end{question}

\section{Report}
The report for this part should consist of the answers to questions 1-\thequestion\ and the results from task 1-\thetask.


\end{document}
