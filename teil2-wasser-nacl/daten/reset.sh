#!/bin/bash

rm -f *.xvg
rm -f *.edr
rm -f *.log
rm -f *.cpt
rm -f *.ndx
rm -f *.tpr
rm -f mdout.mdp
rm -f traj_comp.xtc
rm -f \#*
rm -f confout.gro
rm -f vmdscene.*
